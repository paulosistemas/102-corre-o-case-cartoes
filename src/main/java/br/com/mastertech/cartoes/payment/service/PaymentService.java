package br.com.mastertech.cartoes.payment.service;

import br.com.mastertech.cartoes.creditcard.model.CreditCard;
import br.com.mastertech.cartoes.creditcard.service.CreditCardService;
import br.com.mastertech.cartoes.payment.models.Payment;
import br.com.mastertech.cartoes.payment.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private CreditCardService creditCardService;

    public Payment create(Payment payment) {
        CreditCard creditCard = creditCardService.getById(payment.getCreditCard().getId());

        payment.setCreditCard(creditCard);

        return paymentRepository.save(payment);
    }

    public List<Payment> findAllByCreditCard(Long creditCardId) {
        return paymentRepository.findAllByCreditCard_id(creditCardId);
    }

}
