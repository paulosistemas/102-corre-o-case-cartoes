package br.com.mastertech.cartoes.payment.repository;

import br.com.mastertech.cartoes.creditcard.model.CreditCard;
import br.com.mastertech.cartoes.payment.models.Payment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PaymentRepository extends CrudRepository<Payment, Long> {

    List<Payment> findAllByCreditCard_id(Long creditCardId);
}
