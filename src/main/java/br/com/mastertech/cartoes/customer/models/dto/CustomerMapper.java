package br.com.mastertech.cartoes.customer.models.dto;

import br.com.mastertech.cartoes.customer.models.Customer;
import org.springframework.stereotype.Component;

@Component
public class CustomerMapper {

    public Customer toCustomer(CreateCustomerRequest createCustomerRequest) {
        Customer customer = new Customer();
        customer.setName(createCustomerRequest.getName());
        return customer;
    }

}
