package br.com.mastertech.cartoes.customer.service;

import br.com.mastertech.cartoes.customer.exceptions.CustomerNotFoundException;
import br.com.mastertech.cartoes.customer.models.Customer;
import br.com.mastertech.cartoes.customer.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public Customer create(Customer customer) {
        return customerRepository.save(customer);
    }

    public Customer getById(Long id) {
        Optional<Customer> byId = customerRepository.findById(id);

        if(!byId.isPresent()) {
            throw new CustomerNotFoundException();
        }

        return byId.get();
    }

}
