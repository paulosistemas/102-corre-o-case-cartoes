package br.com.mastertech.cartoes.customer.repository;

import br.com.mastertech.cartoes.customer.models.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
}
